#MACO11O - MiniEP7
#Gustavo Korzune Gurgel - 4778350

#PARTE 1

function compareByValue(x, y)
    order = ['2','3','4','5','6','7','8','9','1','J','Q','K','A']
    value_x = ' '
    value_y = ' '
    for i = 1:length(order)
        
        if x[1] == order[i]
            value_x = i
        end

        if y[1] == order[i]
            value_y = i
        end

    end
    return value_x < value_y
end

using Test

function testcompareByValue()

    @test compareByValue("2♠", "A♠") == true
    @test compareByValue("K♡", "10♡") == false
    @test compareByValue("10♠", "10♡") == false
    @test compareByValue("5♠", "10♡") == true
    println("End of tests")

end

testcompareByValue()

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
        for i in 2:tam
            j = i
            while j > 1
                if compareByValueAndSuit(v[j], v[j-1])
                    troca(v, j, j - 1)
                else
                    break
                end
                j = j - 1
            end
        end
    return v
end

function testInsercaoByValue()
    
    @test insercao(["10♡", "10♢", "K♠", "A♠", "J♠", "A♠"]) == ["10♡", "10♢", "J♠", "K♠", "A♠", "A♠"]
    println("End of tests")

end

testInsercaoByValue()

#PARTE 2

function compareByValueAndSuit(x, y)
    byValue = compareByValue(x, y)
    suit_x = 0
    suit_y = 0
    order = ['♢', '♠', '♡', '♣']

    for i = 1:length(order)
        if x[end] == order[i]
            suit_x = i
        end

        if y[end] == order[i]
            suit_y = i
        end
    end

    return ((suit_x < suit_y) | ((suit_x == suit_y) & (byValue)))
end

function testcompareByValueAndSuit()

    @test compareByValueAndSuit("2♠", "A♠") == true
    @test compareByValueAndSuit("K♡", "10♡") == false
    @test compareByValueAndSuit("10♠", "10♡") == true
    @test compareByValueAndSuit("A♠", "2♡") == true
    println("End of tests")

end

testcompareByValueAndSuit()

function testInsercaoValueAndSuit()
    
    @test insercao(["10♡", "10♢", "K♠", "A♠", "J♠", "A♠"]) == ["10♢", "J♠", "K♠", "A♠", "A♠", "10♡"]
    println("End of tests")

end

testInsercaoValueAndSuit()
